package com.esi.rentit.service;

import com.esi.rentit.dto.maintenance.RepairCostDto;
import com.esi.rentit.models.inventory.PlantInventoryItem;

import java.time.LocalDate;
import java.util.List;

public interface MaintenanceTaskService {
    List<PlantInventoryItem> findPlantsNotBeenScheduledForAnyMaintenanceForLast12Months(LocalDate date);
    List<RepairCostDto> findRepairCostForLast5Year();
}
