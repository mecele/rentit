package com.esi.rentit.service.validation.impl;

import com.esi.rentit.controllers.exceptions.PurchaseOrderNotFoundException;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.repo.sales.PurchaseOrderRepository;
import com.esi.rentit.service.validation.POValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class POValidatorImpl implements POValidator {

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    @Override
    public boolean validate(PurchaseOrder po) {
        LocalDate startDate = po.getRentalPeriod().getStartDate();
        LocalDate endDate = po.getRentalPeriod().getEndDate();

        /*
          Checking if startDate is greater than endDate or startDate&endDate is less than now.
         */
        if(endDate.compareTo(startDate) < 0 || endDate.compareTo(LocalDate.now()) < 0 || startDate.compareTo(LocalDate.now()) < 0 )
            return false;

        return true;

    }

    @Override
    public void validatePOId(Long id) {
        boolean isPresent = purchaseOrderRepository.findById(id).isPresent();
        if (!isPresent){
            throw new PurchaseOrderNotFoundException();
        }
    }
}


