package com.esi.rentit.service.validation;

import com.esi.rentit.models.sales.PurchaseOrder;

public interface POValidator {
    boolean validate(PurchaseOrder po);
    void validatePOId(Long id);
}
