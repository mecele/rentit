package com.esi.rentit.service;

import com.esi.rentit.dto.sales.POExtensionDTO;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.sales.POExtensionRequest;
import com.esi.rentit.models.sales.POStatus;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.models.value.BusinessPeriod;

import java.util.List;

public interface SalesService {
    PurchaseOrder createPurchaseOrder(PlantInventoryEntry entry, BusinessPeriod period);

    List<PurchaseOrder> findPurchaseOrdersByPOStatus(POStatus status);
    PurchaseOrder changeStatusOfPOToDispatched(Long poId);
    PurchaseOrder changeStatusOfPOToReturned(Long poId);
    List<PurchaseOrder> getAllPurchaseOrders();

    PurchaseOrder preAllocatePlantToPO(PurchaseOrder purchaseOrder);
    PurchaseOrder acceptPO(Long id);
    PurchaseOrder rejectPO(Long id);
    PurchaseOrder findPurchaseOrder(Long id);
    POExtensionRequest findPOExtensionRequest(Long id);
    PurchaseOrder acceptRPExtension(Long poId, Long extId);
    PurchaseOrder rejectRPExtension(Long poId, Long extId);
    PurchaseOrder requestExtensionForPO(PurchaseOrder purchaseOrder, POExtensionDTO dto);
}
