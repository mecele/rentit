package com.esi.rentit.service;

import com.esi.rentit.dto.inventory.CorrectiveRepairNumAndRentalNumPerItemDto;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.models.reservation.PlantReservation;
import com.esi.rentit.models.sales.PurchaseOrder;

import java.time.LocalDate;
import java.util.List;

public interface InventoryService {

    void save(PlantInventoryEntry plantInventoryEntry);

    PlantInventoryEntry getPlantInventoryEntry(Long id);

    List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate);

    PlantInventoryEntry findAvailablePlant(Long id, LocalDate startDate, LocalDate endDate);

    List<CorrectiveRepairNumAndRentalNumPerItemDto> findCorrectiveRepairNumAndRentalNumPerItem(LocalDate startDate, LocalDate endDate);

    List<PlantInventoryItem> findAvailablePlantItems(Long id, LocalDate startDate, LocalDate endDate);

    void cancelPlantReservation(PurchaseOrder purchaseOrder);
}
