package com.esi.rentit.service;

import com.esi.rentit.models.employee.Employee;
import com.esi.rentit.models.sales.PurchaseOrder;

import java.util.List;

public interface EmployeeService {

    Employee findEmployeeById(Long id);
    Employee createEmployee(Employee employee);
    List<Employee> findAllEmployees();
    PurchaseOrder changeStatusOfPOtoDispatched(Long poId);
    PurchaseOrder changeStatusOfPOtoReturned(Long poId);
}
