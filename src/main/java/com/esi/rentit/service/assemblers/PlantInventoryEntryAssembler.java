package com.esi.rentit.service.assemblers;

import com.esi.rentit.controllers.rest.SalesRestController;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.dto.inventory.PlantInventoryEntryDTO;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryEntryAssembler
        extends ResourceAssemblerSupport<PlantInventoryEntry, PlantInventoryEntryDTO> {

  public PlantInventoryEntryAssembler() {
    super(SalesRestController
            .class, PlantInventoryEntryDTO.class);
  }

  @Override
  public PlantInventoryEntryDTO toResource(PlantInventoryEntry plant) {
    PlantInventoryEntryDTO dto = createResourceWithId(plant.getId(), plant);
    dto.set_id(plant.getId());
    dto.setName(plant.getName());
    dto.setDescription(plant.getDescription());
    dto.setPrice(plant.getPrice());
    return dto;
  }
}

