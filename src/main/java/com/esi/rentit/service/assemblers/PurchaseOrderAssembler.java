package com.esi.rentit.service.assemblers;
import com.esi.rentit.dto.inventory.BusinessPeriodDTO;
import com.esi.rentit.dto.sales.PurchaseOrderDTO;
import com.esi.rentit.models.sales.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseOrderAssembler {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public PurchaseOrderDTO toResource(PurchaseOrder po) {
        PurchaseOrderDTO dto = new PurchaseOrderDTO();
        dto.set_id(po.getId());
        dto.setPlant(plantInventoryEntryAssembler.toResource(po.getPlant()));
        dto.setRentalPeriod(BusinessPeriodDTO.of(po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate()));
        dto.setTotal(po.getTotal());
        dto.setStatus(po.getStatus());

        return dto;
    }
}