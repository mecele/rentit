package com.esi.rentit.service.impl;


import com.esi.rentit.models.invoicing.Invoice;
import com.esi.rentit.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    public JavaMailSender emailSender;

    @Override
    public void sendInvoice(Invoice invoice, String mailSubject) {

        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo("sebuhi11@gmail.com");
            message.setSubject(mailSubject);
            message.setText(invoice.getDueDate() + " " + invoice.getPurchaseOrder().getId());
            emailSender.send(message);
        } catch (MailException e) {
            e.printStackTrace();
        }
    }
}
