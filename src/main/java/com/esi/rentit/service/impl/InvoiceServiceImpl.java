package com.esi.rentit.service.impl;

import com.esi.rentit.models.invoicing.Invoice;
import com.esi.rentit.models.invoicing.InvoiceStatus;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.repo.invoicing.InvoiceRepository;
import com.esi.rentit.service.EmailService;
import com.esi.rentit.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private EmailService emailService;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Override
    public Invoice createAndSendInvoice(PurchaseOrder purchaseOrder) {
        Invoice invoice = new Invoice();
        invoice.setPurchaseOrder(purchaseOrder);
        invoice.setStatus(InvoiceStatus.UNPAID);
        invoice.setDueDate(purchaseOrder.getRentalPeriod().getEndDate().plusDays(30));
        invoiceRepository.save(invoice);
        emailService.sendInvoice(invoice, "Your invoice has been created!");

        return invoice;
    }

    @Override
    public List<Invoice> getAll() {
        return invoiceRepository.findAll();
    }

    @Override
    public Invoice approveInvoice(Long purchaseOrderId) {
        Invoice invoice = invoiceRepository.findByPurchaseOrderId(purchaseOrderId);
        invoice.setStatus(InvoiceStatus.PAID);
        return invoiceRepository.save(invoice);

    }

    @Override
    @Scheduled(cron = "0 0 9 * * *")
    public void sendInvoiceReminder() {
        try {
            List<Invoice> invoices = invoiceRepository.findAllByDueDateIsBeforeAndStatus(LocalDate.now().plusDays((long) 3), InvoiceStatus.UNPAID);
            for (Invoice invoice : invoices) {
                emailService.sendInvoice(invoice, "REMINDER: Your Rentit Invoice Due Date is soon!");
            }

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }


    }

}
