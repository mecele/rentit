package com.esi.rentit.service.impl;

import com.esi.rentit.dto.maintenance.RepairCostDto;
import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.repo.maintenance.MaintenanceTaskRepository;
import com.esi.rentit.service.MaintenanceTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class MaintenanceTaskServiceImpl implements MaintenanceTaskService {

    @Autowired
    private MaintenanceTaskRepository maintenanceTaskRepository;

    @Override
    public List<PlantInventoryItem> findPlantsNotBeenScheduledForAnyMaintenanceForLast12Months(LocalDate date) {
        return maintenanceTaskRepository.findPlantsNotBeenScheduledForAnyMaintenanceForLast12Months(date);
    }

    @Override
    public List<RepairCostDto> findRepairCostForLast5Year() {
        List<Object[]> result = maintenanceTaskRepository.findRepairCostForLast5Year();
        List<RepairCostDto> repairCostDtoList = new ArrayList<>();

        for (Object[] values : result) {
            RepairCostDto repairCostDto = new RepairCostDto();
            repairCostDto.setYear((Integer) values[0]);
            repairCostDto.setCost((BigDecimal) values[1]);
            repairCostDtoList.add(repairCostDto);
        }

        return repairCostDtoList;
    }
}
