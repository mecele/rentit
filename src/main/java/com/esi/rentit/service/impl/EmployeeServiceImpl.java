package com.esi.rentit.service.impl;

import com.esi.rentit.models.employee.Employee;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.repo.employee.EmployeeRepository;
import com.esi.rentit.service.EmployeeService;
import com.esi.rentit.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SalesService salesService;

    @Override
    public Employee findEmployeeById(Long id) {
        return employeeRepository.findById(id).orElse(null);
    }

    @Override
    public List<Employee> findAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public PurchaseOrder changeStatusOfPOtoDispatched(Long poId) {
        return salesService.changeStatusOfPOToDispatched(poId);
    }

    @Override
    public PurchaseOrder changeStatusOfPOtoReturned(Long poId) {
        return salesService.changeStatusOfPOToReturned(poId);
    }
}
