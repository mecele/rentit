package com.esi.rentit.service.impl;

import com.esi.rentit.dto.inventory.CorrectiveRepairNumAndRentalNumPerItemDto;
import com.esi.rentit.models.inventory.EquipmentCondition;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.models.reservation.PlantReservation;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.repo.inventory.InventoryRepository;
import com.esi.rentit.repo.reservation.PlantReservationRepository;
import com.esi.rentit.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private PlantReservationRepository plantReservationRepository;

    @Override
    public void save(PlantInventoryEntry plantInventoryEntry) {
        inventoryRepository.save(plantInventoryEntry);
    }

    @Override
    public PlantInventoryEntry getPlantInventoryEntry(Long id) {
        return inventoryRepository.findById(id).orElse(null);
    }

    @Override
    public List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        return inventoryRepository.findAvailablePlants(name, startDate, endDate);
    }

    @Override
    public PlantInventoryEntry findAvailablePlant(Long id, LocalDate startDate, LocalDate endDate) {
        PlantInventoryEntry entry = null;
        try {
            entry = inventoryRepository.findAvailablePlant(id, startDate, endDate);
        }catch (Exception e) {

        }

        return entry;
    }

    @Override
    public List<CorrectiveRepairNumAndRentalNumPerItemDto> findCorrectiveRepairNumAndRentalNumPerItem(LocalDate startDate, LocalDate endDate) {
        List<Object[]> result = inventoryRepository.findCorrectiveRepairNumAndRentalNumPerItem(startDate, endDate);
        List<CorrectiveRepairNumAndRentalNumPerItemDto> dtoList = new ArrayList<>();

        for (Object[] values : result) {
            CorrectiveRepairNumAndRentalNumPerItemDto dto = new CorrectiveRepairNumAndRentalNumPerItemDto();
            dto.setPlantInventoryItemId((BigInteger) values[0]);
            dto.setEquipmentCondition(EquipmentCondition.valueOf((String) values[1]));
            dto.setSerialNumber((String) values[2]);
            dto.setPlantInfoId((BigInteger) values[3]);
            dto.setRentalNum((BigInteger) values[4]);
            dto.setCorrectiveTaskNum((BigInteger) values[5]);
            dtoList.add(dto);
        }

        return dtoList;
    }

    @Override
    public List<PlantInventoryItem> findAvailablePlantItems(Long id, LocalDate startDate, LocalDate endDate) {
        return inventoryRepository.findAvailableItems(id, startDate, endDate);
    }

    @Override
    public void cancelPlantReservation(PurchaseOrder purchaseOrder) {
        while (!purchaseOrder.getReservations().isEmpty())
            plantReservationRepository.delete(purchaseOrder.getReservations().remove(0));
    }
}
