package com.esi.rentit.service.impl;

import com.esi.rentit.controllers.exceptions.ResourceNotFoundException;
import com.esi.rentit.dto.sales.POExtensionDTO;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.models.invoicing.Invoice;
import com.esi.rentit.models.reservation.PlantReservation;
import com.esi.rentit.models.sales.ExtStatus;
import com.esi.rentit.models.sales.POExtensionRequest;
import com.esi.rentit.models.sales.POStatus;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.models.value.BusinessPeriod;
import com.esi.rentit.repo.extension.POExtensionRepository;
import com.esi.rentit.repo.invoicing.InvoiceRepository;
import com.esi.rentit.repo.reservation.PlantReservationRepository;
import com.esi.rentit.repo.sales.PurchaseOrderRepository;
import com.esi.rentit.service.InventoryService;
import com.esi.rentit.service.InvoiceService;
import com.esi.rentit.service.SalesService;
import com.esi.rentit.service.validation.POValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class SalesServiceImpl implements SalesService {

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private PlantReservationRepository plantReservationRepository;

    @Autowired
    private POExtensionRepository poExtensionRepository;

    @Autowired
    private POValidator poValidator;

    @Autowired
    private InvoiceService invoiceService;

    @Override
    public PurchaseOrder createPurchaseOrder(PlantInventoryEntry entry, BusinessPeriod period) {
        PurchaseOrder purchaseOrder = PurchaseOrder.of(entry, period);
        return purchaseOrderRepository.save(purchaseOrder);
    }

    @Override
    public List<PurchaseOrder> findPurchaseOrdersByPOStatus(POStatus status) {
        return purchaseOrderRepository.findByStatus(status);
    }

    @Override
    public List<PurchaseOrder> getAllPurchaseOrders() {
        return purchaseOrderRepository.findAll();
    }

    @Override
    public PurchaseOrder changeStatusOfPOToDispatched(Long poId) {
        PurchaseOrder purchaseOrder = findPurchaseOrder(poId);
        purchaseOrder.setStatus(POStatus.PLANT_DISPATCHED);
        return purchaseOrderRepository.save(purchaseOrder);
    }

    @Override
    public PurchaseOrder changeStatusOfPOToReturned(Long poId) {
        PurchaseOrder purchaseOrder = findPurchaseOrder(poId);
        purchaseOrder.setStatus(POStatus.PLANT_RETURNED);
        invoiceService.createAndSendInvoice(purchaseOrder);

        return purchaseOrderRepository.save(purchaseOrder);
    }

    @Override
    public PurchaseOrder preAllocatePlantToPO(PurchaseOrder purchaseOrder) {
        List<PlantInventoryItem> availablePlantItems = inventoryService.findAvailablePlantItems(
                purchaseOrder.getPlant().getId(),
                purchaseOrder.getRentalPeriod().getStartDate(),
                purchaseOrder.getRentalPeriod().getEndDate());

        if (availablePlantItems.size() != 0) {
            PlantReservation plantReservation = new PlantReservation();

            plantReservation.setPlant(availablePlantItems.get(0));
            plantReservation.setRental(purchaseOrder);
            plantReservation.setSchedule(purchaseOrder.getRentalPeriod());
            plantReservationRepository.save(plantReservation);

            purchaseOrder.addReservation(plantReservation);
            purchaseOrder.setStatus(POStatus.PREALLOCATED);
            purchaseOrderRepository.save(purchaseOrder);

            return purchaseOrder;
        } else {
            purchaseOrder.setStatus(POStatus.REJECTED);
            purchaseOrderRepository.save(purchaseOrder);
            return purchaseOrder;
        }
    }

    @Override
    public PurchaseOrder acceptPO(Long id) {
        poValidator.validatePOId(id);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findById(id).get();
        purchaseOrder.setStatus(POStatus.OPEN);

        /*
            cost or other business rules will be implemented here
         */
        return purchaseOrderRepository.save(purchaseOrder);
    }

    @Override
    public PurchaseOrder rejectPO(Long id) {
        poValidator.validatePOId(id);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findById(id).get();
        purchaseOrder.getRentalPeriod().getStartDate();
        // In the following cases plant cancellation cannot be done
        POStatus poStatus = purchaseOrder.getStatus();
        if (poStatus.equals(POStatus.PLANT_DISPATCHED) ||
                poStatus.equals(POStatus.PLANT_RETURNED) ||
                poStatus.equals(POStatus.INVOICED)) {
            return null;
        }
        inventoryService.cancelPlantReservation(purchaseOrder);
        purchaseOrder.setStatus(POStatus.REJECTED);

        return purchaseOrderRepository.save(purchaseOrder);
    }

    @Override
    public PurchaseOrder findPurchaseOrder(Long id) {
        poValidator.validatePOId(id);

        return purchaseOrderRepository.findById(id).get();
    }

    @Override
    public PurchaseOrder requestExtensionForPO(PurchaseOrder po, POExtensionDTO poExtensionDTO) {
        // When plant is not available for the requested period of time return null
        if (inventoryService.findAvailablePlant(po.getPlant().getId(), LocalDate.now(), poExtensionDTO.getExtensionEndDate()) == null) {
            return null;
        }
        if (poExtensionDTO.getExtensionEndDate().compareTo(po.getRentalPeriod().getEndDate()) <= 0) {
            return null;
        }
        POExtensionRequest poExtensionRequest = POExtensionRequest.of(poExtensionDTO, ExtStatus.OPEN);
        poExtensionRepository.save(poExtensionRequest);
        po.addExtensions(poExtensionRequest);
        po.setStatus(POStatus.PENDING_EXTENSION);

        return purchaseOrderRepository.save(po);
    }

    @Override
    public POExtensionRequest findPOExtensionRequest(Long id) {
        return poExtensionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Extension Request with id " + id + " not found"));
    }

    @Override
    public PurchaseOrder acceptRPExtension(Long poId, Long extId) {
        PurchaseOrder purchaseOrder = findPurchaseOrder(poId);
        POExtensionRequest poExtensionRequest = findPOExtensionRequest(extId);
        BusinessPeriod newRentalPeriod = BusinessPeriod.of(purchaseOrder.getRentalPeriod().getStartDate(), poExtensionRequest.getExtensionEndDate());
        poExtensionRequest.setExtStatus(ExtStatus.ACCEPTED);
        poExtensionRepository.save(poExtensionRequest);
        purchaseOrder.setRentalPeriod(newRentalPeriod);
        purchaseOrder.setStatus(POStatus.OPEN);

        return purchaseOrderRepository.save(purchaseOrder);
    }

    @Override
    public PurchaseOrder rejectRPExtension(Long poId, Long extId) {
        PurchaseOrder purchaseOrder = findPurchaseOrder(poId);
        POExtensionRequest poExtensionRequest = findPOExtensionRequest(extId);
        poExtensionRequest.setExtStatus(ExtStatus.REJECTED);
        poExtensionRepository.save(poExtensionRequest);
        // If endDate expires then set status of PO to Closed
        if (LocalDate.now().compareTo(purchaseOrder.getRentalPeriod().getEndDate()) > 0) {
            purchaseOrder.setStatus(POStatus.CLOSED);
        } else {
            purchaseOrder.setStatus(POStatus.OPEN);
        }

        return purchaseOrderRepository.save(purchaseOrder);
    }

}
