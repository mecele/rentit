package com.esi.rentit.service;

import com.esi.rentit.models.invoicing.Invoice;
import com.esi.rentit.models.sales.PurchaseOrder;

import java.util.List;

public interface InvoiceService {

    Invoice createAndSendInvoice(PurchaseOrder purchaseOrder);

    List<Invoice> getAll();

    Invoice approveInvoice(Long purchaseOrderId);

    void sendInvoiceReminder();
}
