package com.esi.rentit.service;

import com.esi.rentit.models.invoicing.Invoice;


public interface EmailService {

    void sendInvoice(Invoice invoice, String mailSubject);
}
