package com.esi.rentit.dto.sales;

import com.esi.rentit.dto.inventory.BusinessPeriodDTO;
import com.esi.rentit.dto.inventory.PlantInventoryEntryDTO;
import com.esi.rentit.models.sales.POStatus;
import lombok.Data;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
public class PurchaseOrderDTO {
    Long _id;
    PlantInventoryEntryDTO plant;
    BusinessPeriodDTO rentalPeriod;

    @Column(precision = 8, scale = 2)
    BigDecimal total;
    POStatus status;
}