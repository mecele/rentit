package com.esi.rentit.dto.inventory;

import com.esi.rentit.models.inventory.EquipmentCondition;

import java.math.BigInteger;

public class CorrectiveRepairNumAndRentalNumPerItemDto {
    private BigInteger plantInventoryItemId;
    private EquipmentCondition equipmentCondition;
    private String serialNumber;
    private BigInteger plantInfoId;
    private BigInteger rentalNum;
    private BigInteger CorrectiveTaskNum;

    public CorrectiveRepairNumAndRentalNumPerItemDto() {
    }

    public CorrectiveRepairNumAndRentalNumPerItemDto(BigInteger plantInventoryItemId, EquipmentCondition equipmentCondition, String serialNumber, BigInteger plantInfoId, BigInteger rentalNum, BigInteger correctiveTaskNum) {
        this.plantInventoryItemId = plantInventoryItemId;
        this.equipmentCondition = equipmentCondition;
        this.serialNumber = serialNumber;
        this.plantInfoId = plantInfoId;
        this.rentalNum = rentalNum;
        CorrectiveTaskNum = correctiveTaskNum;
    }

    public BigInteger getPlantInventoryItemId() {
        return plantInventoryItemId;
    }

    public void setPlantInventoryItemId(BigInteger plantInventoryItemId) {
        this.plantInventoryItemId = plantInventoryItemId;
    }

    public EquipmentCondition getEquipmentCondition() {
        return equipmentCondition;
    }

    public void setEquipmentCondition(EquipmentCondition equipmentCondition) {
        this.equipmentCondition = equipmentCondition;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public BigInteger getPlantInfoId() {
        return plantInfoId;
    }

    public void setPlantInfoId(BigInteger plantInfoId) {
        this.plantInfoId = plantInfoId;
    }

    public BigInteger getRentalNum() {
        return rentalNum;
    }

    public void setRentalNum(BigInteger rentalNum) {
        this.rentalNum = rentalNum;
    }

    public BigInteger getCorrectiveTaskNum() {
        return CorrectiveTaskNum;
    }

    public void setCorrectiveTaskNum(BigInteger correctiveTaskNum) {
        CorrectiveTaskNum = correctiveTaskNum;
    }
}
