package com.esi.rentit.dto.maintenance;


import java.math.BigDecimal;

public class RepairCostDto {
    private int year;
    private BigDecimal cost;

    public RepairCostDto(int year, BigDecimal cost) {
        this.year = year;
        this.cost = cost;
    }

    public RepairCostDto() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
}
