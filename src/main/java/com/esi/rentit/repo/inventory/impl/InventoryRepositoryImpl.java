package com.esi.rentit.repo.inventory.impl;

import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.repo.inventory.CustomInventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

public class InventoryRepositoryImpl implements CustomInventoryRepository {

    @Autowired
    private EntityManager em;

    /**
     * @param name
     * @param startDate
     * @param endDate
     * @return PlantInventoryEntry array
     */
    @Override
    public List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select p.plantInfo from PlantInventoryItem p where LOWER(p.plantInfo.name) like concat('%', ?1, '%') and p not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)",
                PlantInventoryEntry.class)
                .setParameter(1, name.toLowerCase())
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

    @Override
    public PlantInventoryEntry findAvailablePlant(Long itemId, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select p.plantInfo from PlantInventoryItem p where p.id = ?1 and p not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)",
                PlantInventoryEntry.class)
                .setParameter(1, itemId)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getSingleResult();
    }

    /**
     * @param startDate
     * @param endDate
     * @return Below method will return like [ [id, equipment_condition , serial_number , plant_info_id, RENTAL_NUM , CORRECTIVE_TASK_NUM] ... ]
     * i.e: [ [1, SERVICEABLE, A01, 1, 2, 0], ..]
     */
    @Override
    public List<Object[]> findCorrectiveRepairNumAndRentalNumPerItem(LocalDate startDate, LocalDate endDate) {
        List<Object[]> results = em.createNativeQuery(
                "SELECT  PIIC.ID, " +
                        "        PIIC.EQUIPMENT_CONDITION, " +
                        "        PIIC.SERIAL_NUMBER, " +
                        "        PIIC.PLANT_INFO_ID, " +
                        "        COALESCE(R.RENTAL_NUM , 0) AS RENTAL_NUM , " +
                        "        COALESCE(PIIC.CORRECTIVE_TASK_NUM, 0) AS CORRECTIVE_TASK_NUM " +
                        " FROM  " +
                        "(SELECT PII.*, C.CORRECTIVE_TASK_NUM FROM  " +
                        "(SELECT MP.PLANT_INVENTORY_ITEM_ID AS ID,  " +
                        "           COUNT(MT.TYPE_OF_WORK ) AS CORRECTIVE_TASK_NUM " +
                        "FROM MAINTENANCE_PLAN MP  " +
                        "INNER JOIN MAINTENANCE_PLAN_TASKS MPT " +
                        "INNER JOIN MAINTENANCE_TASK MT  " +
                        "INNER JOIN PLANT_INVENTORY_ITEM PII " +
                        "        ON MP.ID = MPT.MAINTENANCE_PLAN_ID   " +
                        "       AND MP.PLANT_INVENTORY_ITEM_ID  = PII.ID " +
                        "       AND MT.ID=MPT.TASKS_ID " +
                        "WHERE  MT.TYPE_OF_WORK = 'CORRECTIVE'  AND " +
                        "       ((MT.START_DATE >= ?1 AND MT.START_DATE <= ?2 ) " +
                        "   OR " +
                        "       (MT.END_DATE  <= ?2 AND MT.END_DATE  >= ?1)) " +
                        "GROUP BY MP.PLANT_INVENTORY_ITEM_ID) " +
                        "AS C RIGHT OUTER JOIN  PLANT_INVENTORY_ITEM PII " +
                        "ON PII.ID = C.ID) " +
                        "AS PIIC LEFT OUTER JOIN  " +
                        "(" +
                        "SELECT PII.ID,  " +
                        "        COUNT(*) AS RENTAL_NUM " +
                        "FROM PLANT_INVENTORY_ITEM PII " +
                        "INNER JOIN  PLANT_RESERVATION PR " +
                        "INNER JOIN PURCHASE_ORDER PO " +
                        "ON PR.RENTAL_ID = PO.ID  " +
                        "AND PII.ID = PR.PLANT_ID " +
//                            "WHERE  PO.STATUS IN ('OPEN', 'CLOSED') AND" +
                        " WHERE ((PR.START_DATE >= ?1 AND PR.START_DATE <= ?2 ) " +
                        "   OR (PR.END_DATE  <= ?2 AND PR.END_DATE  >= ?1)) " +
                        "GROUP BY PII.ID" +
                        ") AS R " +
                        "ON R.ID = PIIC.ID " +
                        "ORDER BY 5, 6; "
        ).setParameter(1, startDate).setParameter(2, endDate).getResultList();

        return results;
    }

    @Override
    public List<PlantInventoryItem> findAvailableItems(Long id, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select p from PlantInventoryItem p where p.plantInfo.id=?1 and p not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)",
                PlantInventoryItem.class)
                .setParameter(1, id)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }
}