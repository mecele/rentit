package com.esi.rentit.repo.inventory;

import com.esi.rentit.models.inventory.PlantInventoryEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, Long> {
    List<PlantInventoryEntry> findByNameContaining(String str);
}
