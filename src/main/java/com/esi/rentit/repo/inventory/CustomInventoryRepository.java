package com.esi.rentit.repo.inventory;

import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.inventory.PlantInventoryItem;

import java.time.LocalDate;
import java.util.List;

// TODO: Why not @Query annotation here in order to execute custom query.

public interface CustomInventoryRepository {
    List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate);
    PlantInventoryEntry findAvailablePlant(Long id, LocalDate startDate, LocalDate endDate);
    List<Object[]> findCorrectiveRepairNumAndRentalNumPerItem(LocalDate startDate, LocalDate endDate);
    List<PlantInventoryItem> findAvailableItems(Long id, LocalDate startDate, LocalDate endDate);
}
