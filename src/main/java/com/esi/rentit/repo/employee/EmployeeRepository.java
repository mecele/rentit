package com.esi.rentit.repo.employee;

import com.esi.rentit.models.employee.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
