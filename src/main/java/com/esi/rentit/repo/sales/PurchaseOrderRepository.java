package com.esi.rentit.repo.sales;

import com.esi.rentit.models.sales.POStatus;
import com.esi.rentit.models.sales.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {

    List<PurchaseOrder> findByStatus(POStatus status);
}
