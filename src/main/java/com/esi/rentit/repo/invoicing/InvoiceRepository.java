package com.esi.rentit.repo.invoicing;

import com.esi.rentit.models.invoicing.Invoice;
import com.esi.rentit.models.invoicing.InvoiceStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    Invoice findByPurchaseOrderId(Long poId);

    List<Invoice> findAllByDueDateIsBeforeAndStatus(LocalDate localDate, InvoiceStatus status);


}
