package com.esi.rentit.repo.extension;

import com.esi.rentit.models.sales.POExtensionRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface POExtensionRepository extends JpaRepository<POExtensionRequest, Long> {
}
