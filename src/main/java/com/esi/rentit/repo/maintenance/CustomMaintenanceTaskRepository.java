package com.esi.rentit.repo.maintenance;

import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.models.maintenance.MaintenanceTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;


public interface CustomMaintenanceTaskRepository {
    List<PlantInventoryItem> findPlantsNotBeenScheduledForAnyMaintenanceForLast12Months(LocalDate date);
    List<Object []> findRepairCostForLast5Year();

}
