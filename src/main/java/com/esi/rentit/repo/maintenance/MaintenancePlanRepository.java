package com.esi.rentit.repo.maintenance;

import com.esi.rentit.models.maintenance.MaintenancePlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaintenancePlanRepository extends JpaRepository<MaintenancePlan, Long> {


}
