package com.esi.rentit.repo.maintenance;

import com.esi.rentit.models.maintenance.MaintenanceTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaintenanceTaskRepository extends JpaRepository<MaintenanceTask, Long>, CustomMaintenanceTaskRepository {
}
