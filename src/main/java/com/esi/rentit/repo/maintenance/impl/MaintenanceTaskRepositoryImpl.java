package com.esi.rentit.repo.maintenance.impl;

import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.repo.maintenance.CustomMaintenanceTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

public class MaintenanceTaskRepositoryImpl implements CustomMaintenanceTaskRepository {

    @Autowired
    EntityManager em;

    public List<PlantInventoryItem> findPlantsNotBeenScheduledForAnyMaintenanceForLast12Months(LocalDate date) {
        return em.createQuery("select task.plantReservation.plant FROM MaintenanceTask task"
                +
                " WHERE task.schedule.endDate < ?1")
                .setParameter(1, date.minusMonths(12))
                .getResultList();
    }

    @Override
    public List<Object[]> findRepairCostForLast5Year() {
        List<Object[]> results = em.createNativeQuery(
                "SELECT              YEAR(END_DATE)      AS YEAR," +
                        "                        SUM(PRICE)          AS REPAIR_COST" +
                        "    FROM                MAINTENANCE_TASK" +
                        "    WHERE               TYPE_OF_WORK = 'PREVENTIVE' AND" +
                        "                        DATEDIFF(YEAR, END_DATE , CURRENT_DATE) < 6 AND" +
                        "                        END_DATE  <=CURRENT_DATE" +
                        "    GROUP BY        YEAR(END_DATE) " +
                        "    LIMIT 6").getResultList();

        return results;

    }
}