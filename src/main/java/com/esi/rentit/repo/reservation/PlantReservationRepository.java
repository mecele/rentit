package com.esi.rentit.repo.reservation;

import com.esi.rentit.models.reservation.PlantReservation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlantReservationRepository extends JpaRepository<PlantReservation, Long> {
}
