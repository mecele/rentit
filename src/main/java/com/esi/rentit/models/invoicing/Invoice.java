package com.esi.rentit.models.invoicing;

import com.esi.rentit.models.sales.PurchaseOrder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate dueDate;

    @OneToOne
    private PurchaseOrder purchaseOrder;

    @Enumerated(EnumType.STRING)
    private InvoiceStatus status;



}
