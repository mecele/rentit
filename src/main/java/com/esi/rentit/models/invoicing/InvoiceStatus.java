package com.esi.rentit.models.invoicing;

public enum InvoiceStatus {
    PAID, UNPAID
}
