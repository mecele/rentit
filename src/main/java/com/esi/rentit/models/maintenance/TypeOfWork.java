package com.esi.rentit.models.maintenance;

public enum TypeOfWork {
    PREVENTIVE, CORRECTIVE, OPERATIVE;
}
