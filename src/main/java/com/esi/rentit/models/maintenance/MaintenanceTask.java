package com.esi.rentit.models.maintenance;

import com.esi.rentit.models.reservation.PlantReservation;
import com.esi.rentit.models.value.BusinessPeriod;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class MaintenanceTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;

    @Enumerated(EnumType.STRING)
    private TypeOfWork typeOfWork;

    @Embedded
    private BusinessPeriod schedule;

    @Column(precision = 8, scale = 2)
    private BigDecimal price;

    @ManyToOne
    private PlantReservation plantReservation;




}
