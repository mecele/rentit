package com.esi.rentit.models.maintenance;

import com.esi.rentit.models.inventory.PlantInventoryItem;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class MaintenancePlan {

    @Id
    @GeneratedValue
    private Long id;
    private Integer yearOfAction;

    @OneToMany(cascade={CascadeType.ALL})
    private List<MaintenanceTask> tasks;

    @ManyToOne
    private PlantInventoryItem plantInventoryItem;
}
