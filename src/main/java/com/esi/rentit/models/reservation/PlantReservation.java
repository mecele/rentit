package com.esi.rentit.models.reservation;

import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.models.maintenance.MaintenancePlan;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.models.value.BusinessPeriod;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlantReservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private BusinessPeriod schedule;

    @ManyToOne
    private PlantInventoryItem plant;

    @ManyToOne
    private MaintenancePlan maintenancePlan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private PurchaseOrder rental;

}
