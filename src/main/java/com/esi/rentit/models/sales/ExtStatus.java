package com.esi.rentit.models.sales;

public enum ExtStatus {
    OPEN, ACCEPTED, REJECTED
}
