package com.esi.rentit.models.sales;

import com.esi.rentit.dto.sales.POExtensionDTO;
import lombok.*;
import sun.rmi.server.LoaderHandler;

import javax.persistence.*;
import java.time.LocalDate;


@Data
@Entity
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class POExtensionRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate extensionEndDate;

    @Enumerated(EnumType.STRING)
    private ExtStatus extStatus;

    public static POExtensionRequest of(POExtensionDTO poExtensionDTO, ExtStatus status){
        POExtensionRequest poExtensionRequest = new POExtensionRequest();
        poExtensionRequest.extensionEndDate = poExtensionDTO.getExtensionEndDate();
        poExtensionRequest.extStatus = status;

        return poExtensionRequest;
    }
}
