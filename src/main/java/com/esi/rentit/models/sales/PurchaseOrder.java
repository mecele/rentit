package com.esi.rentit.models.sales;

import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.reservation.PlantReservation;
import com.esi.rentit.models.value.BusinessPeriod;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PurchaseOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(fetch = FetchType.LAZY)
    private List<PlantReservation> reservations;

    @OneToMany(fetch = FetchType.LAZY)
    private List<POExtensionRequest> poExtensionRequests;

    @ManyToOne(fetch = FetchType.EAGER)
    private PlantInventoryEntry plant;
    private LocalDate issueDate;
    private LocalDate paymentSchedule;

    @Column(precision = 8, scale = 2)
    private BigDecimal total;

    @Enumerated(EnumType.STRING)
    private POStatus status;

    @Embedded
    private BusinessPeriod rentalPeriod;

    public static PurchaseOrder of(PlantInventoryEntry entry, BusinessPeriod period) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = entry;
        po.rentalPeriod = period;
        po.reservations = new ArrayList<>();
        po.issueDate = LocalDate.now();
        po.status = POStatus.PENDING;
        return po;
    }

    public void addReservation(PlantReservation reservation) {
        reservations.add(reservation);
    }

    public void addExtensions(POExtensionRequest extension) {
        poExtensionRequests.add(extension);
    }

}
