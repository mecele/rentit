package com.esi.rentit.models.inventory;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

//TODO: why attributes are not private access?

@Entity
@Data
public class PlantInventoryEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;

    @Column(precision = 8, scale = 2)
    private BigDecimal price;
}
