package com.esi.rentit.models.inventory;

public enum EquipmentCondition {
    SERVICEABLE, UNSERVICEABLE_REPAIRABLE, UNSERVICEABLE_INCOMPLETE, UNSERVICEABLE_CONDEMNED
}
