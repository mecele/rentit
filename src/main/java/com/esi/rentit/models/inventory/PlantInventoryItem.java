package com.esi.rentit.models.inventory;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlantInventoryItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    private EquipmentCondition equipmentCondition;

    @ManyToOne
    private PlantInventoryEntry plantInfo;
}
