package com.esi.rentit.controllers.exceptions;

public class InvalidRentalPeriodException extends RuntimeException {
    private static final long serialVersionUID = 1677312800028340316L;
}
