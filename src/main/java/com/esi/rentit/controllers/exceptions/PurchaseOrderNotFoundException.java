package com.esi.rentit.controllers.exceptions;

public class PurchaseOrderNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 3945321685669749820L;
}
