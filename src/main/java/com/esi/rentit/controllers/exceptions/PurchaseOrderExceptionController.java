package com.esi.rentit.controllers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class PurchaseOrderExceptionController {

    @ExceptionHandler(value = InvalidRentalPeriodException.class)
    public ResponseEntity<Object> handleInvalidRentalPeriodException(InvalidRentalPeriodException exception) {
        return new ResponseEntity<>("Invalid Rental Period", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = PurchaseOrderNotFoundException.class)
    public ResponseEntity<Object> handlePurchaseOrderNotFoundException(PurchaseOrderNotFoundException exception) {
        return new ResponseEntity<>("Purchase Order Not Found", HttpStatus.NOT_FOUND);
    }
}

