package com.esi.rentit.controllers.rest;

import com.esi.rentit.models.employee.Employee;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees/{id}")
    public Employee findEmployeeById(@PathVariable Long id) {
        return employeeService.findEmployeeById(id);
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeService.findAllEmployees();
    }

    @PostMapping("/employees")
    public Employee findEmployeeById(@RequestBody Employee employee) {
        return employeeService.createEmployee(employee);
    }

    @PostMapping("/changeStatusToDispatched/{orderId}")
    public PurchaseOrder changePurchaseOrderStatusToDispatched(@PathVariable Long orderId) {
        return employeeService.changeStatusOfPOtoDispatched(orderId);
    }

    @PostMapping("/changeStatusToReturned/{orderId}")
    public PurchaseOrder changePurchaseOrderStatusToReturned(@PathVariable Long orderId) {
        return employeeService.changeStatusOfPOtoReturned(orderId);
    }

}
