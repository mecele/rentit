package com.esi.rentit.controllers.rest;

import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/inventory")
public class InventoryRestController {

    @Autowired
    private InventoryService inventoryService;


    @GetMapping("/plants")
    public List<PlantInventoryEntry> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.findAvailablePlants(plantName, startDate, endDate);
    }

    @GetMapping("/plant/{id}")
    public PlantInventoryEntry getPlant(@PathVariable("id") Long id) {
        return inventoryService.getPlantInventoryEntry(id);
    }

    @GetMapping("/plants/{id}")
    public PlantInventoryEntry findAvailablePlant(
            @PathVariable("id") Long id,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return inventoryService.findAvailablePlant(id, startDate, endDate);
    }

}
