package com.esi.rentit.controllers.rest;

import com.esi.rentit.models.invoicing.Invoice;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceRestController {

    @Autowired
    private InvoiceService invoiceService;

    @GetMapping
    public List<Invoice> getInvoices() {
        return invoiceService.getAll();
    }

    @PostMapping("/{purchaseOrderId}")
    public Invoice approveInvoice(@PathVariable("purchaseOrderId") Long purchaseOrderId) {
        return invoiceService.approveInvoice(purchaseOrderId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Invoice createInvoice(@RequestBody PurchaseOrder purchaseOrder) {
        return invoiceService.createAndSendInvoice(purchaseOrder);
    }
}
