package com.esi.rentit.controllers.rest;

import com.esi.rentit.controllers.exceptions.InvalidRentalPeriodException;
import com.esi.rentit.dto.sales.POExtensionDTO;
import com.esi.rentit.models.sales.POExtensionRequest;
import com.esi.rentit.models.sales.POStatus;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.service.assemblers.PlantInventoryEntryAssembler;
import com.esi.rentit.service.assemblers.PurchaseOrderAssembler;
import com.esi.rentit.service.SalesService;
import com.esi.rentit.service.validation.POValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@RestController
@CrossOrigin
@RequestMapping("/api/sales")
public class SalesRestController {

    @Autowired
    private SalesService salesService;

    @Autowired
    private PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    private PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    private POValidator poValidator;

    @GetMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrder> pendingPOs() {
        return salesService.findPurchaseOrdersByPOStatus(POStatus.PENDING);
    }


    @GetMapping("/allOrders")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrder> getAllOrders() {
        return salesService.getAllPurchaseOrders();
    }

    @GetMapping("/extensionOrders")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrder> pendingExtensionPOs() {
        return salesService.findPurchaseOrdersByPOStatus(POStatus.PENDING_EXTENSION);
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrder fetchPurchaseOrder(@PathVariable("id") Long id) {
        return salesService.findPurchaseOrder(id);
    }

    @PostMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> preallocateItemToPO(@PathVariable("id") Long id) {
        PurchaseOrder newlyCreatedPO = salesService.findPurchaseOrder(id);

        PurchaseOrder po = salesService.preAllocatePlantToPO(newlyCreatedPO);

        return new ResponseEntity<>(po, HttpStatus.CREATED);
    }

    @PostMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> createPurchaseOrder(@RequestBody PurchaseOrder po) {

        if (!poValidator.validate(po)) throw new InvalidRentalPeriodException();
        PurchaseOrder newPO = salesService.createPurchaseOrder(po.getPlant(), po.getRentalPeriod());

        HttpHeaders headers = new HttpHeaders();

        return new ResponseEntity<>(
                new Resource<>(newPO,
                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(newPO.getId()))
                                .withSelfRel()),
                headers, HttpStatus.CREATED);

    }


//    @GetMapping("/orders/{id}/extensions")
//    public List<POExtensionDTO> retrievePurchaseOrderExtensions(@PathVariable("id") Long id) {
//        List<Resource<POExtensionDTO>> result = new ArrayList<>();
//        POExtensionDTO extension = new POExtensionDTO();
//        extension.setEndDate(LocalDate.now().plusWeeks(1));
//
//        result.add(new Resource<>(extension));
//        return result;
//    }

    @PostMapping("/orders/{id}/extensions")
    public ResponseEntity<?> requestPurchaseOrderExtension(@RequestBody POExtensionDTO extension, @PathVariable("id") Long id) {
        PurchaseOrder purchaseOrder = salesService.findPurchaseOrder(id);

        return new ResponseEntity<>(salesService.requestExtensionForPO(purchaseOrder, extension), HttpStatus.OK);
    }

    @PostMapping("/orders/{poId}/extensions/{extId}/accept")
    public ResponseEntity<?> acceptRPExtension(@PathVariable("poId") Long poId,
                                               @PathVariable("extId") Long extId) {

        return new ResponseEntity<>(salesService.acceptRPExtension(poId, extId), HttpStatus.OK);
    }

    @DeleteMapping("/orders/{poId}/extensions/{extId}/accept")
    public ResponseEntity<?> rejectRPExtension(@PathVariable("poId") Long poId,
                                               @PathVariable("extId") Long extId) {

        return new ResponseEntity<>(salesService.rejectRPExtension(poId, extId), HttpStatus.OK);
    }

    @PostMapping("/orders/{id}/accept")
    public ResponseEntity<?> acceptPO(@PathVariable("id") Long id) {
        return new ResponseEntity<>(salesService.acceptPO(id), HttpStatus.OK);
    }

    @DeleteMapping("/orders/{id}/accept")
    public ResponseEntity<?> rejectPO(@PathVariable("id") Long id) {
        return new ResponseEntity<>(salesService.rejectPO(id), HttpStatus.OK);
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<?> closePO(@PathVariable("id") Long id) {
        return new ResponseEntity<>(salesService.rejectPO(id), HttpStatus.OK);
    }

}
