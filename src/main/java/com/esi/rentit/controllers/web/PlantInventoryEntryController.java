package com.esi.rentit.controllers.web;

import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.repo.inventory.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;

@Controller
public class PlantInventoryEntryController {

    @Autowired
    private InventoryRepository repo;

    @GetMapping("/plants")
    public String list(Model model) {
        model.addAttribute("plants", repo.findAvailablePlants("exc", LocalDate.now(), LocalDate.now()));
        return "plants/list";
    }

    @GetMapping(value = "/plants/form")
    public String form(Model model) {
        model.addAttribute("plant", new PlantInventoryEntry());
        return "plants/create";
    }

    @PostMapping(value = "/plants")
    public String create(PlantInventoryEntry plant) {
        repo.save(plant);
        return "redirect:/plants";
    }

}
