Feature: Creation of Purchase Order
  As a Rentit's customer
  So that I start with the construction project
  I want to query RentIt's plant catalog
  I want to select one plant for creating a Plant hire request
  I want to be able to reject the plant hire request

  Background: Plant catalog
    Given the following plant catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
      |  4 | Midi Excavator | 8 Tonne Midi excavator           | 300.00 |
      |  5 | Maxi Excavator | 15 Tonne Large excavator         | 400.00 |
      |  6 | Maxi Excavator | 20 Tonne Large excavator         | 450.00 |
      |  7 | HS Dumper      | 1.5 Tonne Hi-Swivel Dumper       | 150.00 |
      |  8 | FT Dumper      | 2 Tonne Front Tip Dumper         | 180.00 |

    And a customer is in the "Plant Catalog" web page

    Scenario: Reject the plant hire request
      Given the customer queries the plant catalog for an "exc" available from "002019/05/25" to "002019/05/27"
      Then 3 plants are shown
      When the customer selects a "3 Tonne Mini excavator"
      Then a purchase order should be created
      When the customer queries the request list
      Then 1 request is shown
      When the customer clicks reject
      Then 0 request is shown
