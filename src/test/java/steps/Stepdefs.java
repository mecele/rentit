package steps;

import com.esi.rentit.models.inventory.PlantInventoryEntry;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class Stepdefs {
    WebDriver customer;
    RestTemplate restTemplate = new RestTemplate();

    static {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\poocoder\\Downloads\\chromedriver_win32\\chromedriver.exe");
    }

    @Before
    public void setup() {
        customer = new ChromeDriver();
    }

    @After
    public void tearoff() {
        // Comment the following line if you want to check the application's final state on the browser
        customer.close();
    }

    static PlantInventoryEntry of(String id, String name, String description, String price) {
        PlantInventoryEntry entry = new PlantInventoryEntry();
        entry.setName(name);
        entry.setId(Long.valueOf(id));
        entry.setDescription(description);
        entry.setPrice(new BigDecimal(price));

        return entry;
    }

    @Given("^the following plant catalog$")
    public void the_following_plant_catalog(DataTable table) throws Exception {
        List<PlantInventoryEntry> entries = table.asMaps(String.class, String.class)
                .stream()
                .map(row -> of(row.get("id"), row.get("name"), row.get("description"), row.get("price")))
                .collect(Collectors.toList());

        //PlantInventoryEntry[] result = restTemplate.postForObject("http://localhost:8080/api/entries", entries, PlantInventoryEntry[].class);
    }

    @Given("^the following inventory$")
    public void the_following_inventory(DataTable items) throws Exception {
    }

    @Given("^a customer is in the \"([^\"]*)\" web page$")
    public void a_customer_is_in_the_web_page(String arg1) throws Exception {
        if (arg1.contains("RentIt")) {
            customer.get("http://localhost:8083/#/");
        } else {
            customer.get("http://localhost:8082/#/");
        }
    }

    @Given("^no purchase order exists in the system$")
    public void no_purchase_order_exists_in_the_system() throws Exception {
    }

    //---Log in----
    @When("^the \"([^\"]*)\" logs in$")
    public void the_user_logs_in(String user) throws Exception {
        customer.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div[1]/div/input")).sendKeys(user);
        customer.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div[2]/div/input")).sendKeys(user);
        customer.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/a")).click();

    }

    /////////////////////////////////////////////////////////////////////////
    @Then("^\"([^\"]*)\" is logged in$")
    public void is_logged_in(String user) throws Exception {
        Thread.sleep(3000);
        assertThat("//*[@id=\"nav\"]/a[1]").isNotEmpty();
    }

    // ---Search for a plant ---

    @When("^the customer queries the plant catalog for an \"([^\"]*)\" available from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void the_customer_queries_the_plant_catalog_for_an_available_from_to(String plantName, String startDate, String endDate) throws Exception {
        customer.findElement(By.id("queryName")).sendKeys(plantName);
        customer.findElement(By.id("startDate")).click();
        //Click departure day
        WebElement rpw = customer.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/div/section/div[1]/section/div[2]/div/div/div[3]/div/div/div/section/div/div[4]/a[7]"));
        rpw.click();
        Thread.sleep(3000);
        customer.findElement(By.id("endDate")).click();
        Thread.sleep(3000);
        rpw = customer.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/div/section/div[1]/section/div[3]/div/div/div[3]/div/div/div/section/div/div[5]/a[4]"));
        rpw.click();
        Thread.sleep(3000);
        customer.findElement(By.id("submitQuery")).click();
    }

    //---Works Engineer approves po

    @When("^the worksEngineer clicks \"([^\"]*)\"$")
    public void the_worksengineer_clicks_label(String label) throws InterruptedException {
        Thread.sleep(3000);

        List<?> rows = customer.findElements(By.id("requestData"));

        if (label.contains("Approve")) {
            WebElement rpw = customer.findElement(By.xpath("//*[@id=\"approve\"]"));
            rpw.click();
        }
        if (label.contains("Reject")) {
            WebElement rpw = customer.findElement(By.xpath("//*[@id=\"commentBox\"]"));
            rpw.sendKeys("Some comment");
            rpw = customer.findElement(By.xpath("//*[@id=\"rejectReq\"]"));
            rpw.click();
        }
    }

    //---Update PO---

    @When("^customer updates po to \"([^\"]*)\" \"([^\"]*)\"$")
    public void the_customer_updates_po(String engineer, String site) throws Exception {
        customer.findElement(By.xpath("//*[@id=\"updateReq\"]")).click();
        Thread.sleep(3000);
        customer.findElement(By.id("siteEngineer")).sendKeys(engineer);
        Thread.sleep(3000);
        customer.findElement(By.id("constructionSite")).sendKeys(site);
        WebElement button = customer.findElement(By.id("update"));
        Thread.sleep(3000);
        button.click();
    }

    @Then("^columns are changed to \"([^\"]*)\" \"([^\"]*)\"$")
    public void columns_are_changed_to(String engineer, String site) throws Exception {
        Thread.sleep(3000);

        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", "Mini excavator")));
        WebElement siteEngineer = row.findElement(By.xpath("//*[@id=\"requestData\"]/tbody/tr[2]/td[2]"));
        WebElement constructionSite = row.findElement(By.xpath("//*[@id=\"requestData\"]/tbody/tr[2]/td[3]"));

        assertThat(siteEngineer.getText()).isEqualTo(engineer);
        assertThat(constructionSite.getText()).isEqualTo(site);

    }


    //---mark return---

    @When("^the customer marks invoice returned$")
    public void mark_invoice_returned() throws Exception {
        WebElement button = customer.findElement(By.xpath("//*[@id=\"reject\"]"));
        Thread.sleep(3000);
        button.click();
    }

    @Then("^list of pos seen$")
    public void list_of_pos_seen() throws Exception {

        WebElement row = customer.findElement(By.xpath("//*[@id=\"orders\"]/tbody/tr[1]/td[7]"));

        assertThat(row).isNotNull();

    }

    //--Approve invoice--
    @When("^the customer approves invoice$")
    public void approve_invoice() throws Exception {
        Thread.sleep(1000);
        customer.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/div/nav/ul/li[4]/a/span")).click();
        WebElement invoice = customer.findElement(By.xpath("//*[@id=\"invoice\"]"));
        invoice.click();
        WebElement button = customer.findElement(By.xpath("//*[@id=\"approveInvoice\"]"));
        button.click();
    }

    @Then("^the invoice is approved$")
    public void invoice_is_approved() throws Exception {
        WebElement row = customer.findElement(By.xpath("//*[@id=\"requestData\"]/tbody/tr[1]/td[9]"));
        assertThat(row).isNotNull();

    }

    //////////////
    //////////////
    @Then("^(\\d+) plants are shown$")
    public void plants_are_shown(int numberOfPlants) throws Exception {
        Thread.sleep(3000);
        List<?> rows = customer.findElements(By.id("queryResult"));
        assertThat(rows).isNotEmpty();
    }

    @Then("^a purchase order should be created$")
    public void a_purchase_order_should_be_created() throws InterruptedException {
        customer.findElement(By.id("siteEngineerName")).sendKeys("a");
        customer.findElement(By.id("constructionSiteName")).sendKeys("a");
        customer.findElement(By.id("submitHReq")).click();

    }

    @Then("^(\\d+) request is shown$")
    public void request_is_shown(int numberOfRequests) throws InterruptedException {
        Thread.sleep(3000);
        List<?> rows = customer.findElements(By.id("requestData"));
        assertThat(rows).isNotEmpty();
    }

    @When("^the customer queries the request list$")
    public void the_customer_queries_the_request_list() throws InterruptedException {
        Thread.sleep(3000);
        customer.findElement(By.xpath("//*[@id=\"app\"]/div[2]/div/div/div/nav/ul/li[4]/a/span")).click();

    }

    @When("^the customer clicks reject$")
    public void the_customer_clicks_reject() {
        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", "Mini excavator")));
        WebElement rejectPlantButton = row.findElement(By.xpath("//a[contains(text(), 'Reject')]"));
        rejectPlantButton.click();

        customer.findElement(By.id("commentBox")).sendKeys("commentBox");
        customer.findElement(By.id("rejectReq")).click();


    }

    @When("^the customer clicks accept$")
    public void the_customer_clicks_accept() {
        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", "Mini excavator")));
        WebElement acceptPlantButton = row.findElement(By.xpath("//a[contains(text(), 'Accept')]"));
        acceptPlantButton.click();

    }

    @Then("^the request status is \"([^\"]*)\"$")
    public void the_request_status_is(String status_title) throws InterruptedException {
        Thread.sleep(3000);

        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", "Mini excavator")));
        WebElement status = row.findElement(By.xpath("//*[@id=\"requestData\"]/tbody/tr[1]/td[4]"));

        assertThat(status.getText()).isEqualTo(status_title);

    }

    @Then("^the request status is REJECTED")
    public void the_request_status_is_rejected() throws InterruptedException {
        Thread.sleep(3000);

        customer.findElement(By.id("commentBox")).sendKeys("commentBox");
        customer.findElement(By.id("rejectReq")).click();

        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", "Mini excavator")));
        WebElement status = row.findElement(By.xpath("//a[contains(text(), 'REJECTED')]"));

        assertThat(status).isNotNull();


    }


    @When("^the customer selects a \"([^\"]*)\"$")
    public void the_customer_selects_a(String plantDescription) throws Exception {
        WebElement row = customer.findElement(By.xpath(String.format("//tr/td[contains(text(), '%s')]", plantDescription)));

        WebElement selectPlantButton = row.findElement(By.xpath("//a[contains(text(), 'Select plant')]"));
        selectPlantButton.click();
    }

}
