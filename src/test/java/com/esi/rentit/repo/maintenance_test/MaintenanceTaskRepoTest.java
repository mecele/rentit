package com.esi.rentit.repo.maintenance_test;
import com.esi.rentit.RentitApplication;
import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.repo.inventory.PlantInventoryItemRepository;
import com.esi.rentit.repo.maintenance.MaintenanceTaskRepository;
import org.apache.commons.lang3.ArrayUtils;
import org.assertj.core.api.ListAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class)
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)

public class MaintenanceTaskRepoTest {

    @Autowired
    MaintenanceTaskRepository maintenanceTaskRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;


    @Test
    public void findPlantsNotBeenScheduledForAnyMaintenanceForLast12MonthsTest() {

        PlantInventoryItem entryItem = plantInventoryItemRepository.findById(1l).get();



        List<PlantInventoryItem> plants = maintenanceTaskRepository.findPlantsNotBeenScheduledForAnyMaintenanceForLast12Months(LocalDate.of(2019, 3 ,1));

        assertThat(plants.get(0)).isEqualTo(entryItem);
        assertThat(plants.get(1)).isEqualTo(entryItem);

        assertThat(maintenanceTaskRepository.findPlantsNotBeenScheduledForAnyMaintenanceForLast12Months(LocalDate.of(2019, 3, 1))
                .size())
                .isEqualTo(2);


    }

    @Test
    public void findRepairCostForLast5YearTest(){
        // This list of object arrays should be like this [[2017, 600], ...]]
        List<Object[]> yearsAndCosts = maintenanceTaskRepository.findRepairCostForLast5Year();
        Object[] year2017 = yearsAndCosts.get(0);

        assertThat(ArrayUtils.contains(year2017, 2017)).isTrue();
        assertThat(ArrayUtils.contains(year2017, new BigDecimal(600).setScale(2))).isTrue();
    }
}
