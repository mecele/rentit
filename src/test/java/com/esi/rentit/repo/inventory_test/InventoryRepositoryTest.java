package com.esi.rentit.repo.inventory_test;

import com.esi.rentit.RentitApplication;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.repo.inventory.InventoryRepository;
import com.esi.rentit.repo.inventory.PlantInventoryEntryRepository;
import com.esi.rentit.repo.inventory.PlantInventoryItemRepository;
import com.esi.rentit.repo.reservation.PlantReservationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

//import sun.net.www.content.text.plain;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class)
//@Sql(scripts = "import.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InventoryRepositoryTest {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepo;

    @Autowired
    PlantInventoryItemRepository plantInventorItemRepo;

    @Autowired
    PlantReservationRepository plantReservationRepo;

    @Autowired
    InventoryRepository inventoryRepo;

    @Test
    public void queryPlantCatalog() {
        assertThat(plantInventoryEntryRepo.count()).isEqualTo(14l);
    }

    @Test
    public void queryByName() {
        assertThat(plantInventoryEntryRepo.findByNameContaining("Mini").size()).isEqualTo(2);
    }

    @Test
    public void findAvailablePlantsTest() {
        //Validation of number of available plants
        List<PlantInventoryEntry> plants = inventoryRepo.findAvailablePlants("exc", LocalDate.now(), LocalDate.now());
        PlantInventoryEntry entry = plantInventoryEntryRepo.findById(1l).orElse(new PlantInventoryEntry());
        //Validation of returned object
        //plants.get(0) first object of the list must return first entry in the import.sql
        assertThat(plants.get(0).getName()).contains(entry.getName());

    }

    @Test
    public void findCorrectiveRepairNumAndRentalNumPerItem() {
        LocalDate startDate = LocalDate.of(2017, 11, 5);
        LocalDate endDate = LocalDate.of(2019, 11, 5);

        List<Object[]> itemListWithCorrRepAndRentalNum = inventoryRepo.findCorrectiveRepairNumAndRentalNumPerItem(startDate, endDate);
        Object[] platNo1 = itemListWithCorrRepAndRentalNum.get(4);
        int rentalNum = 2;
        int correctiveNum = 0;
        for (Object[] objects : itemListWithCorrRepAndRentalNum) {
            if (objects[0].equals(1)) {
                assertThat(rentalNum).isEqualTo(platNo1[5]);
                assertThat(correctiveNum).isEqualTo(platNo1[4]);
            }
        }


    }
}