package com.esi.rentit.controller;

import java.io.IOException;

import com.esi.rentit.RentitApplication;
import com.esi.rentit.repo.inventory.PlantInventoryEntryRepository;
import com.esi.rentit.repo.sales.PurchaseOrderRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class)
@WebAppConfiguration
public abstract class AbstractTest {
   protected MockMvc mvc;

   @Autowired
   WebApplicationContext webApplicationContext;

   @Autowired
   PlantInventoryEntryRepository plantInventoryEntryRepository;

   @Autowired
   ObjectMapper objectMapper;

   @Autowired
   PurchaseOrderRepository purchaseOrderRepository;

   protected void setUp() {
      mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
   }
   protected String mapToJson(Object obj) throws JsonProcessingException {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(obj);
   }
}