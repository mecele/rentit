package com.esi.rentit.controller;

import com.esi.rentit.RentitApplication;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.models.value.BusinessPeriod;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = RentitApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SalesControllerTest extends AbstractTest {
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }


    @Transactional
    public PlantInventoryEntry createPlantInventoryEntry() {
        PlantInventoryEntry plantInventoryEntry = new PlantInventoryEntry();
        plantInventoryEntry.setName("Mini excavator");
        plantInventoryEntry.setDescription("1.5 Tonne Mini excavator");
        plantInventoryEntry.setPrice(new BigDecimal(150));

        return plantInventoryEntryRepository.save(plantInventoryEntry);
    }

    @Transactional
    public PurchaseOrder createPurchaseOrder() {
        PlantInventoryEntry plantInventoryEntry = createPlantInventoryEntry();
        BusinessPeriod businessPeriod = BusinessPeriod.of(LocalDate.of(2019, 5, 5), LocalDate.of(2019, 10, 10));

        PurchaseOrder purchaseOrder = PurchaseOrder.of(plantInventoryEntry, businessPeriod);


        return purchaseOrderRepository.save(purchaseOrder);
    }

    @Test
    public void createPurchaseOrderWhenRentalPeriodIsInPast() throws Exception {
        String uri = "/api/sales/orders";

        //when startDate and EndDate < Now
        String poWithInvalidRentalPeriod = "{\n" +
                "\t\"plant\":{\n" +
                "\t\"id\": 1,\n" +
                "\t\"name\": \"Mini excavator\",\n" +
                "\t\"description\": \"1.5 Tonne Mini excavator\",\n" +
                "\t\"price\": 150\n" +
                "\t},\n" +
                "\t\"reservations\": [\n" +
                "\t\t{\n" +
                "\t\t\"id\": 1\n" +
                "\t\t\n" +
                "\t\t}\n" +
                "\t],\n" +
                "\t\"rentalPeriod\": {\n" +
                "\t\t\"startDate\": \"2018-04-15\",\n" +
                "\t\t\"endDate\": \"2018-02-21\"\n" +
                "\t\n" +
                "\t}\n" +
                "\n" +
                "\n" +
                "}";

        MvcResult mvcResult = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(poWithInvalidRentalPeriod)).andReturn();


        int status = mvcResult.getResponse().getStatus();

        assertEquals(400, status);


    }

    @Test
    public void createPurchaseOrderWhenStartDateGreaterThanEndDate() throws Exception {
        String uri = "/api/sales/orders";

        //when startDate > endDate
        String poWithInvalidRentalPeriod = "{\n" +
                "\t\"plant\":{\n" +
                "\t\"id\": 1,\n" +
                "\t\"name\": \"Mini excavator\",\n" +
                "\t\"description\": \"1.5 Tonne Mini excavator\",\n" +
                "\t\"price\": 150\n" +
                "\t},\n" +
                "\t\"reservations\": [\n" +
                "\t\t{\n" +
                "\t\t\"id\": 1\n" +
                "\t\t\n" +
                "\t\t}\n" +
                "\t],\n" +
                "\t\"rentalPeriod\": {\n" +
                "\t\t\"startDate\": \"2020-04-15\",\n" +
                "\t\t\"endDate\": \"2019-04-21\"\n" +
                "\t\n" +
                "\t}\n" +
                "\n" +
                "\n" +
                "}";

        MvcResult mvcResult = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(poWithInvalidRentalPeriod)).andReturn();


        int status = mvcResult.getResponse().getStatus();

        assertEquals(400, status);


    }

    @Test
    public void createPurchaseOrderWhenValidRentalPeriod() throws Exception {
        String uri = "/api/sales/orders";

        createPlantInventoryEntry();

        //when startDate > endDate
        String poWithInvalidRentalPeriod = "{\n" +
                "\t\"plant\":{\n" +
                "\t\"id\": 1,\n" +
                "\t\"name\": \"Mini excavator\",\n" +
                "\t\"description\": \"1.5 Tonne Mini excavator\",\n" +
                "\t\"price\": 150\n" +
                "\t},\n" +
                "\t\"reservations\": [\n" +
                "\t\t\n" +
                "\t],\n" +
                "\t\"rentalPeriod\": {\n" +
                "\t\t\"startDate\": \"2019-04-15\",\n" +
                "\t\t\"endDate\": \"2019-04-21\"\n" +
                "\t\n" +
                "\t}\n" +
                "\n" +
                "\n" +
                "}";

        MvcResult mvcResult = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(poWithInvalidRentalPeriod)).andReturn();


        int status = mvcResult.getResponse().getStatus();

        String purchaseOrderAsString = mvcResult.getResponse().getContentAsString();

        PurchaseOrder purchaseOrder = objectMapper.readValue(purchaseOrderAsString, PurchaseOrder.class);

        assertEquals(201, status);
        assertThat(purchaseOrder.getRentalPeriod().getStartDate()).isEqualTo(LocalDate.of(2019, 4, 15));
        assertThat(purchaseOrder.getRentalPeriod().getEndDate()).isEqualTo(LocalDate.of(2019, 4, 21));
        assertThat(purchaseOrder.getPlant().getName()).isEqualTo("Mini excavator");


    }

    @Test
    public void fetchPurchaseOrderWhenGivenInvalidPOId() throws Exception {
        //PurchaseOrder purchaseOrder = createPurchaseOrder();
        String uri = "/api/sales/orders/-1";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);


    }

    @Test
    public void fetchPurchaseOrderWhenGivenValidPOId() throws Exception {
        PurchaseOrder purchaseOrder = createPurchaseOrder();
        String uri = "/api/sales/orders/" + purchaseOrder.getId();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String fetchedPurchaseOrderAsString = mvcResult.getResponse().getContentAsString();

        PurchaseOrder fetchedPurchaseOrder = objectMapper.readValue(fetchedPurchaseOrderAsString, PurchaseOrder.class);
        assertThat(fetchedPurchaseOrder.getId()).isEqualTo(purchaseOrder.getId());


    }

    /**
     * For the below unit tests we don't need to handle WithValidId condition.
     * Because acceptPO and rejectPO controllers will directly returning the service methods.
     * We have already tested services in their own test classes if they're functioning correctly.
     */

    @Test
    public void acceptPOWithInvalidId() throws Exception {
        String uri = "/api/sales/orders/-1/accept";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);


    }

    @Test
    public void rejectPOWithInvalidId() throws Exception {
        String uri = "/api/sales/orders/-1/accept";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);


    }


}
