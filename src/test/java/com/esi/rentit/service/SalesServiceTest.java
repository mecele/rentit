package com.esi.rentit.service;

import com.esi.rentit.RentitApplication;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.models.sales.POStatus;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.models.value.BusinessPeriod;
import com.esi.rentit.repo.inventory.PlantInventoryEntryRepository;
import com.esi.rentit.repo.inventory.PlantInventoryItemRepository;
import com.esi.rentit.repo.sales.PurchaseOrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class)
//@Sql(scripts = "import.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SalesServiceTest {


    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    SalesService salesService;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    InventoryService inventoryService;


    @Test
    @Transactional
    public void createPurchaseOrder(){
        PlantInventoryEntry plantInventoryEntry = new PlantInventoryEntry();
        plantInventoryEntry.setPrice(new BigDecimal(321));
        plantInventoryEntry.setDescription("desc");
        plantInventoryEntry.setName("exc");

        plantInventoryEntryRepository.save(plantInventoryEntry);

        BusinessPeriod businessPeriod = BusinessPeriod.of(LocalDate.now(), LocalDate.now());



        PurchaseOrder purchaseOrderFromDatabase = salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);

        assertThat(purchaseOrderFromDatabase.getPlant().getName()).isEqualTo("exc");
        assertThat(purchaseOrderFromDatabase.getPlant().getDescription()).isEqualTo("desc");
        assertThat(purchaseOrderFromDatabase.getPlant().getPrice()).isEqualTo("321");
        assertThat(purchaseOrderFromDatabase.getRentalPeriod().getStartDate()).isEqualTo(LocalDate.now());
        assertThat(purchaseOrderFromDatabase.getRentalPeriod().getEndDate()).isEqualTo(LocalDate.now());
        assertThat(purchaseOrderFromDatabase.getStatus()).isEqualTo(POStatus.PENDING);
        assertThat(purchaseOrderFromDatabase.getReservations()).isEmpty();
        assertThat(purchaseOrderFromDatabase.getIssueDate()).isEqualTo(LocalDate.now());

    }

    @Test
    @Transactional
    public void findPurchaseOrderByPOStatus(){
        PlantInventoryEntry plantInventoryEntry = new PlantInventoryEntry();
        plantInventoryEntry.setPrice(new BigDecimal(321));
        plantInventoryEntry.setDescription("desc");
        plantInventoryEntry.setName("exc");

        plantInventoryEntryRepository.save(plantInventoryEntry);

        BusinessPeriod businessPeriod = BusinessPeriod.of(LocalDate.now(), LocalDate.now());



        //PENDING
        salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);
        //PENDING1
        PurchaseOrder purchaseOrderPRE = salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);
        //PREALLOCATE
        PurchaseOrder purchaseOrderREJECT = salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);
        //REJECT
        PurchaseOrder purchaseOrderOPEN = salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);

        purchaseOrderPRE.setStatus(POStatus.PREALLOCATED);
        purchaseOrderREJECT.setStatus(POStatus.REJECTED);
        purchaseOrderOPEN.setStatus(POStatus.OPEN);



        List<PurchaseOrder> list = salesService.findPurchaseOrdersByPOStatus(POStatus.PENDING);

        for(PurchaseOrder purchaseOrder : list){
            assertThat(purchaseOrder.getStatus()).isEqualTo(POStatus.PENDING);
        }





    }

    @Test
    @Transactional
    public void rejectPO() {
        PlantInventoryEntry plantInventoryEntry3 = new PlantInventoryEntry();
        BusinessPeriod businessPeriod1 = BusinessPeriod.of(LocalDate.of(2018, 3, 18), LocalDate.of(2018, 4, 5));

        salesService.createPurchaseOrder(plantInventoryEntry3, businessPeriod1);

        PurchaseOrder purchaseOrder3 = purchaseOrderRepository.getOne((long) 1);

        salesService.rejectPO(purchaseOrder3.getId());
        assertThat(purchaseOrder3.getStatus()).isEqualTo(POStatus.REJECTED);
    }

    @Test
    @Transactional
    public void preAllocatePlantToPO() {
        PlantInventoryEntry plantInventoryEntry = new PlantInventoryEntry();

        plantInventoryEntryRepository.save(plantInventoryEntry);

        PlantInventoryItem plantInventoryItem = new PlantInventoryItem();
        plantInventoryItemRepository.save(plantInventoryItem);
        plantInventoryItem.setPlantInfo(plantInventoryEntry);

        PlantInventoryItem plantInventoryItem1 = new PlantInventoryItem();
        plantInventoryItemRepository.save(plantInventoryItem1);
        plantInventoryItem1.setPlantInfo(plantInventoryEntry);


        BusinessPeriod businessPeriod = BusinessPeriod.of(LocalDate.of(2018, 2, 23), LocalDate.of(2018, 3, 23));

        PurchaseOrder purchaseOrder = salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);
        PurchaseOrder purchaseOrder1 = salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);
        PurchaseOrder purchaseOrder2 = salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);

        salesService.preAllocatePlantToPO(purchaseOrder);
        assertThat(purchaseOrder.getStatus()).isEqualTo(POStatus.PREALLOCATED);
        salesService.preAllocatePlantToPO(purchaseOrder1);
        assertThat(purchaseOrder1.getStatus()).isEqualTo(POStatus.PREALLOCATED);
        salesService.preAllocatePlantToPO(purchaseOrder2);
        assertThat(purchaseOrder2.getStatus()).isEqualTo(POStatus.REJECTED);
    }

    @Test
    @Transactional
    public void acceptPO() {
        PlantInventoryEntry plantInventoryEntry = new PlantInventoryEntry();
        plantInventoryEntryRepository.save(plantInventoryEntry);

        BusinessPeriod businessPeriod = BusinessPeriod.of(LocalDate.of(2018, 2, 23), LocalDate.of(2018, 3, 23));
        PurchaseOrder purchaseOrder = salesService.createPurchaseOrder(plantInventoryEntry, businessPeriod);

        assertThat(salesService.acceptPO(purchaseOrder.getId()).getStatus()).isEqualTo(POStatus.OPEN);
    }
}
