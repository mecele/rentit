package com.esi.rentit.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import com.esi.rentit.RentitApplication;
import com.esi.rentit.dto.maintenance.RepairCostDto;
import com.esi.rentit.models.maintenance.MaintenanceTask;
import com.esi.rentit.models.maintenance.TypeOfWork;
import com.esi.rentit.models.value.BusinessPeriod;
import com.esi.rentit.repo.maintenance.MaintenanceTaskRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class)
//@Sql(scripts = "import.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MaintenanceTaskServiceTest {

    @Autowired
    private MaintenanceTaskService maintenanceTaskService;

    @Autowired
    private MaintenanceTaskRepository maintenanceTaskRepository;

    @Test
    public void findRepairCostForLast5Year() {

        BusinessPeriod businessPeriod = BusinessPeriod.of(LocalDate.of(2019, 9, 1), LocalDate.of(2019, 10, 1));
        BusinessPeriod businessPeriod1 = BusinessPeriod.of(LocalDate.of(2010, 1, 1), LocalDate.of(2011, 1, 1));
        BusinessPeriod businessPeriod2 = BusinessPeriod.of(LocalDate.of(2018, 1, 1), LocalDate.of(2019, 3, 1));
        BusinessPeriod businessPeriod3 = BusinessPeriod.of(LocalDate.of(2015, 1, 1), LocalDate.of(2017, 1, 1));


        MaintenanceTask maintenanceTask = new MaintenanceTask();
        maintenanceTask.setSchedule(businessPeriod); // 2019
        maintenanceTask.setPrice(new BigDecimal(100));
        maintenanceTask.setTypeOfWork(TypeOfWork.PREVENTIVE);

        MaintenanceTask maintenanceTask1 = new MaintenanceTask();
        maintenanceTask1.setSchedule(businessPeriod1); // 2011
        maintenanceTask1.setPrice(new BigDecimal(100));
        maintenanceTask1.setTypeOfWork(TypeOfWork.PREVENTIVE);

        MaintenanceTask maintenanceTask2 = new MaintenanceTask();
        maintenanceTask2.setSchedule(businessPeriod2); // 2019
        maintenanceTask2.setPrice(new BigDecimal(100));
        maintenanceTask2.setTypeOfWork(TypeOfWork.PREVENTIVE);

        MaintenanceTask maintenanceTask3 = new MaintenanceTask();
        maintenanceTask3.setSchedule(businessPeriod3); // 2017
        maintenanceTask3.setPrice(new BigDecimal(100));
        maintenanceTask3.setTypeOfWork(TypeOfWork.PREVENTIVE);

        MaintenanceTask maintenanceTask4 = new MaintenanceTask();
        maintenanceTask4.setSchedule(businessPeriod3);
        maintenanceTask4.setPrice(new BigDecimal(100));
        maintenanceTask4.setTypeOfWork(TypeOfWork.CORRECTIVE);

        maintenanceTaskRepository.save(maintenanceTask);
        maintenanceTaskRepository.save(maintenanceTask1);
        maintenanceTaskRepository.save(maintenanceTask2);
        maintenanceTaskRepository.save(maintenanceTask3);
        maintenanceTaskRepository.save(maintenanceTask4);


        List<RepairCostDto> result = maintenanceTaskService.findRepairCostForLast5Year();

        assertThat(result.get(0).getYear()).isEqualTo(2017);
        assertEquals(result.get(0).getCost().setScale(2), new BigDecimal(100).setScale(2));

        assertThat(result.get(1).getYear()).isEqualTo(2019);
        assertEquals(result.get(1).getCost().setScale(2), new BigDecimal(100).setScale(2));


    }
}
