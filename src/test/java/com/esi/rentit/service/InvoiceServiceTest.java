package com.esi.rentit.service;

import com.esi.rentit.RentitApplication;
import com.esi.rentit.models.invoicing.Invoice;
import com.esi.rentit.repo.invoicing.InvoiceRepository;
import com.esi.rentit.repo.sales.PurchaseOrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class)
//@Sql(scripts = "import.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InvoiceServiceTest {

    @Autowired
    private EmailService emailService;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;


    @Test
    public void repoIntegrationTest() {



        Invoice invoice = invoiceRepository.findByPurchaseOrderId((long) 1);

        System.out.println(invoice.getStatus());

    }
}
