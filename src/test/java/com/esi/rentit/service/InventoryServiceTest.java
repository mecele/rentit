package com.esi.rentit.service;

import com.esi.rentit.RentitApplication;
import com.esi.rentit.dto.inventory.CorrectiveRepairNumAndRentalNumPerItemDto;
import com.esi.rentit.models.inventory.EquipmentCondition;
import com.esi.rentit.models.inventory.PlantInventoryEntry;
import com.esi.rentit.models.inventory.PlantInventoryItem;
import com.esi.rentit.models.reservation.PlantReservation;
import com.esi.rentit.models.sales.PurchaseOrder;
import com.esi.rentit.models.value.BusinessPeriod;
import com.esi.rentit.repo.inventory.PlantInventoryEntryRepository;
import com.esi.rentit.repo.inventory.PlantInventoryItemRepository;
import com.esi.rentit.repo.reservation.PlantReservationRepository;
import com.esi.rentit.repo.sales.PurchaseOrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentitApplication.class)
//@Sql(scripts = "import.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InventoryServiceTest {

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    private PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private PlantReservationRepository plantReservationRepository;

    @Autowired
    private SalesService salesService;

    /**
     * Below test creates 2 entries and 3 items. Saves them to database.
     * Then runs the InventoryService in order to test if DTOs carry the repository queries correctly.
     * <p>
     * first entry has two items and items are valid for the query.
     * second entry has one item but that item is not valid for the query.
     * So in that case return size must be 1 and it must refer to entry1.
     * <p>
     * See the findAvailablePlants repository query and service for further details.
     */
    @Test
    public void findAvailablePlants() {
        PlantInventoryEntry entry1 = new PlantInventoryEntry();
        entry1.setName("hello");
        entry1.setDescription("desc");
        entry1.setPrice(new BigDecimal(231));

        PlantInventoryEntry entry2 = new PlantInventoryEntry();
        entry2.setName("hellooooworld");
        entry2.setDescription("desccccc");
        entry2.setPrice(new BigDecimal(4454));

        PlantInventoryItem item = new PlantInventoryItem();
        item.setPlantInfo(entry1);
        item.setEquipmentCondition(EquipmentCondition.SERVICEABLE);
        item.setSerialNumber("sadsa");

        PlantInventoryItem item1 = new PlantInventoryItem();
        item1.setPlantInfo(entry1);
        item1.setEquipmentCondition(EquipmentCondition.SERVICEABLE);
        item1.setSerialNumber("jfndj");

        PlantInventoryItem item2 = new PlantInventoryItem();
        item2.setPlantInfo(entry2);
        item2.setEquipmentCondition(EquipmentCondition.UNSERVICEABLE_CONDEMNED);
        item2.setSerialNumber("mmmfm");

        inventoryService.save(entry1);
        inventoryService.save(entry2);
        plantInventoryItemRepository.save(item);
        plantInventoryItemRepository.save(item1);
        plantInventoryItemRepository.save(item2);


        List<PlantInventoryEntry> result = inventoryService.findAvailablePlants("hel", LocalDate.now(), LocalDate.now());

        assertThat(result.size()).isEqualTo(3);
        assertThat(result.get(0).getName()).isEqualTo("hello");


    }

    @Test
    public void findCorrectiveRepairNumAndRentalNumPerItem() {
        List<CorrectiveRepairNumAndRentalNumPerItemDto> dtoList = inventoryService.findCorrectiveRepairNumAndRentalNumPerItem(LocalDate.of(2017, 11, 5), LocalDate.of(2019, 11, 5));

        for (CorrectiveRepairNumAndRentalNumPerItemDto dto : dtoList) {
            System.out.println("PlantInfoID: " + dto.getPlantInfoId()
                    + " Serial Number:  " + dto.getSerialNumber()
                    + " CorrectiveTaskNum: " + dto.getCorrectiveTaskNum()
                    + " EquipmnentCondition: " + dto.getEquipmentCondition()
                    + " PlantInvetoryItemId: " + dto.getPlantInventoryItemId()
                    + " Rental Num: " + dto.getRentalNum());
        }
    }

    @Test
    @Transactional
    public void cancelPlantReservation() {
        PlantInventoryEntry entry1 = new PlantInventoryEntry();
        entry1.setName("hello");
        entry1.setDescription("desc");
        entry1.setPrice(new BigDecimal(231));

        plantInventoryEntryRepository.save(entry1);

        BusinessPeriod businessPeriod = BusinessPeriod.of(LocalDate.now(), LocalDate.now());

        PurchaseOrder purchaseOrder = PurchaseOrder.of(entry1, businessPeriod);

        purchaseOrderRepository.save(purchaseOrder);

        PlantReservation plantReservation = new PlantReservation();
        plantReservation.setSchedule(businessPeriod);
        plantReservation.setRental(purchaseOrder);

        PlantReservation plantReservation1 = new PlantReservation();
        plantReservation1.setSchedule(businessPeriod);
        plantReservation1.setRental(purchaseOrder);

        plantReservationRepository.save(plantReservation);
        plantReservationRepository.save(plantReservation1);


        purchaseOrder.getReservations().add(plantReservation);
        purchaseOrder.getReservations().add(plantReservation1);

        purchaseOrderRepository.save(purchaseOrder);


        inventoryService.cancelPlantReservation(purchaseOrder);

        assertThat(salesService.findPurchaseOrder(purchaseOrder.getId()).getReservations().size()).isEqualTo(0);


    }

}
