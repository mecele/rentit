FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/rentit-0.0.1-SNAPSHOT.jar rentit.jar
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/rentit.jar"]